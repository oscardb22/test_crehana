from mongoengine import Document, StringField, ListField, IntField, DateTimeField, ReferenceField, CASCADE


class EventsVideo(Document):
    date = DateTimeField()
    minute = IntField()


class EventsVideoReproduction(Document):
    inscriptions = IntField(required=True)
    start_video = IntField(required=True)
    pause = ListField(ReferenceField(EventsVideo, reverse_delete_rule=CASCADE))
    reproduction = ListField(ReferenceField(EventsVideo, reverse_delete_rule=CASCADE))
    end_video = IntField(required=True)
