from django.contrib.auth import get_user_model
from django.db.models import Model, CharField, ForeignKey, SET_NULL, IntegerField, DateTimeField


class Category(Model):
    name = CharField()

    def __str__(self):
        return f"{self.name}"


class Course(Model):
    tittle = CharField()
    category = ForeignKey(Category, on_delete=SET_NULL)

    def __str__(self):
        return f"{self.tittle}"


class Inscriptions(Model):
    course = ForeignKey(Course, on_delete=SET_NULL)
    user = ForeignKey(get_user_model(), on_delete=SET_NULL)

    def __str__(self):
        return f"{self.id}"


class ProgressVideo(Model):
    inscriptions = ForeignKey(Inscriptions, on_delete=SET_NULL)
    daily_reproduction_minutes = IntegerField()
    day = DateTimeField()

    def __str__(self):
        return f"{self.day} - [{self.daily_reproduction_minutes}]"
