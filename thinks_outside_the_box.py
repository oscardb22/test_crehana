def plus_find_y(x_data, y_data):
    before = None
    for after in x_data:
        if before is None:
            before = after
        else:
            if before + after == y_data:
                return [before, after]
            before = after
    return None


if __name__ == '__main__':
    x = [1, 5, 8, 2, 9]
    y = 10
    print(plus_find_y(x, y), flush=True)
